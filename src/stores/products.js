import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useProductsStore = defineStore('products', () => {

    const products = ref([{ id: 1, name: 'Apples', crossedOut: false}, { id: 2, name: 'Oranges', crossedOut: false}, { id: 3, name: 'Bananas', crossedOut: false}])

    const product = ref('')

    const productsOnLocalStorage = localStorage.getItem("products")
    if (productsOnLocalStorage) {
        products.value = JSON.parse(productsOnLocalStorage)._value
    }

    function addProduct() {
        if (!products.value.find(e => e.name === product.value) && product.value !== '') {
        products.value.push({ id: (new Date()).getTime(), name: product.value, crossedOut: false})
        localStorage.setItem('products', JSON.stringify(products))                
      }
      product.value = ''
    }
    
    function crossProduct(item) {
    
    products.value = products.value.filter(i => i !== item)
    products.value.push({ id: item.id, name: item.name, crossedOut: true})
    
    localStorage.setItem('products', JSON.stringify(products))
    }

    return { products, product, addProduct, crossProduct }
})
